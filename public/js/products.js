
/*var categories = [
    {
        name: 'Phones', link:'#', children:
        [
            { 
                name: 'Nokia', link:'#', products:[
                    {
                        id:'1',
                        name: 'Nokia 5',
                        manufacturer: 'China',
                        price: 5000,
                        image:'product.jpg',
                        attributes: {
                            camera: '5Mp',
                            diagonal: '5',
                            os: 'android'
                        }
                    },
                    {
                        id:'2',
                        name: 'Nokia 6',
                        manufacturer: 'China',
                        price: 8000,
                        image:'product.jpg',
                        attributes: {
                            camera: '5Mp',
                            diagonal: '5,5',
                            os: 'android'
                        }
                    },
                    {
                        id:'3',
                        name: 'Nokia 8',
                        manufacturer: 'China',
                        price: 12000,
                        image:'product.jpg',
                        attributes: {
                            camera: '5Mp',
                            diagonal: '6',
                            os: 'android'
                        }
                    },

            ]
        },
        ]
    },
    {
        name: 'Laptops', link:'#', children:
        [
            { 
                name: 'Acer', link:'#', products:[
                    {
                        id:'4',
                        name: 'Acer Aspire',
                        manufacturer: 'China',
                        price: 15000,
                        image:'product.jpg',
                        attributes: {
                            camera: '5Mp',
                            diagonal: '15,6',
                            os: 'Windows'
                        }
                    },
                    {
                        id:'5',
                        name: 'Acer Aspire 2',
                        manufacturer: 'China',
                        price: 25000,
                        image:'product.jpg',
                        attributes: {
                            camera: '5Mp',
                            diagonal: '15,6',
                            os: 'Windows'
                        }
                    },
                    {
                        id:'6',
                        name: 'Acer Aspire 3',
                        manufacturer: 'China',
                        price: 40000,
                        image:'product.jpg',
                        attributes: {
                            camera: '15Mp',
                            diagonal: '17,2',
                            os: 'Windows'
                        }
                    },

                ]
            },
            { 
            name: 'Samsung', link:'#', products:[
                {
                    id:'7',
                    name: 'Samsung R50',
                    manufacturer: 'China',
                    price: 18000,
                    image:'product.jpg',
                    attributes: {
                        camera: '5Mp',
                        diagonal: '15,6',
                        os: 'Windows'
                    }
                },
                {
                    id:'8',
                    name: 'Samsung R52',
                    manufacturer: 'China',
                    price: 35000,
                    image:'product.jpg',
                    attributes: {
                        camera: '5Mp',
                        diagonal: '15,6',
                        os: 'Windows'
                    }
                },
                {
                    id:'9',
                    name: 'Samsung R57',
                    manufacturer: 'China',
                    price: 45000,
                    image:'product.jpg',
                    attributes: {
                        camera: '15Mp',
                        diagonal: '17,2',
                        os: 'Windows'
                    }
                },

            ]
        }
        ]
    }
];
*/




var products = [
    
    {
        id:'1',
        name: 'Nokia 5',
        manufacturer: 'China',
        price: 5000,
        image:'product.jpg',
        attributes: {
            camera: '5Mp',
            diagonal: '5',
            os: 'android'
        }
    },

    {
        id:'2',
        name: 'Nokia 6',
        manufacturer: 'China',
        price: 8000,
        image:'product.jpg',
        attributes: {
            camera: '5Mp',
            diagonal: '5,5',
            os: 'android'
        }
    },

    {
        id:'3',
        name: 'Nokia 8',
        manufacturer: 'China',
        price: 12000,
        image:'product.jpg',
        attributes: {
            camera: '5Mp',
            diagonal: '6',
            os: 'android'
        }
    },

    {
        id:'4',
        name: 'Acer Aspire',
        manufacturer: 'China',
        price: 15000,
        image:'product.jpg',
        attributes: {
            camera: '5Mp',
            diagonal: '15,6',
            os: 'Windows'
        }
    },

    {
        id:'5',
        name: 'Acer Aspire 2',
        manufacturer: 'China',
        price: 25000,
        image:'product.jpg',
        attributes: {
            camera: '5Mp',
            diagonal: '15,6',
            os: 'Windows'
        }
    },

    {
        id:'6',
        name: 'Acer Aspire 3',
        manufacturer: 'China',
        price: 40000,
        image:'product.jpg',
        attributes: {
            camera: '15Mp',
            diagonal: '17,2',
            os: 'Windows'
        }
    },
    
    {
        id:'7',
        name: 'Samsung R57',
        manufacturer: 'China',
        price: 45000,
        image:'product.jpg',
        attributes: {
            camera: '15Mp',
            diagonal: '17,2',
            os: 'Windows'
        }
    },

    {
        id:'8',
        name: 'Samsung R50',
        manufacturer: 'China',
        price: 18000,
        image:'product.jpg',
        attributes: {
            camera: '5Mp',
            diagonal: '15,6',
            os: 'Windows'
        }
    },
    
    {
        id:'9',
        name: 'Samsung R52',
        manufacturer: 'China',
        price: 35000,
        image:'product.jpg',
        attributes: {
            camera: '5Mp',
            diagonal: '15,6',
            os: 'Windows'
        }
    },
]
