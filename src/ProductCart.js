
var Product = require('./Product');

class ProductCart extends Product{

    constructor(dataProduct, callback) {
        super(dataProduct, callback);
    }

    render() {

        this.container.className = 'cart-product col-12 d-flex align-items-center mt-1 list-group-item-dark';       
        this.button.innerHTML = 'remove cart';
        this.name.className += ' col-sm-3';
        this.price.className += ' col-sm-3 mb-0';
        // this.imgWrap = document.createElement('div');
        // this.imgWrap.className = 'col-sm-3';
        // this.imgWrap.appendChild(this.image);

        this.quantity = document.createElement('div');
        
        this.quantity.className = 'col-sm-2';
        this.quantity.innerHTML = this.data.quantity;

        // this.container.appendChild(this.imgWrap);
        this.price.innerHTML = this.data.sum;
        this.container.appendChild(this.name);
        this.container.appendChild(this.quantity);
        this.container.appendChild(this.price);
        this.container.appendChild(this.button);

        return this.container;
    }

    
};
module.exports = ProductCart;