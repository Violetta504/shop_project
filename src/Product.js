
class Product{
    constructor(dataProduct, callback) {

        this.data = dataProduct;
        this.container = document.createElement('div');
        this.name = document.createElement('h4');
        this.image = document.createElement('img');
        this.price = document.createElement('p');
        this.button = document.createElement('button');

        this.name.className = 'p-name';
        this.image.className = 'p-img img-fluid';
        this.price.className = 'p-price';
        this.button.className = 'btn btn-outline-danger';
        this.button.type = 'button';

        this.name.innerHTML = this.data.name; //Взято из product.js
        this.image.src = this.data.image; //Взято из product.js
        this.price.innerHTML = this.data.price; //Взято из product.js  
        this.button.setAttribute('data-product-id', this.data.id);
        
        if(callback){
            this.button.onclick = callback;
        }
    }

}
module.exports = Product;